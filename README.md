PySenML
=======

~~*pysenml* is a library for parsing and validating SenML messages.~~

This project came to a halt after the release of draft-jennings-core-senml-00
and it has now become obsolete.

[senml-python](https://github.com/eistec/senml-python/) is a good replacement.