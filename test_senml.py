import time
import unittest
import warnings
import json

try:
    from unittest.mock import patch
except ImportError:
    from mock import patch

from schema import SchemaError

import senml
from senml import Entry
from senml import Message


class TestDecodeJSON(unittest.TestCase):

    def test_broken_message(self):
        self.assertRaises(ValueError, senml.decode_json, 'NOT JSON')
        self.assertRaises(ValueError, senml.decode_json, '{}')
        self.assertRaises(ValueError, senml.decode_json, '{"e": []}')

    def test_valid_message(self):
        self.assertIsNotNone(senml.decode_json('{"e":[{"n":"test","v":1.0}]}'))


class TestEncodeJSON(unittest.TestCase):

    def test_encode_json(self):
        entries = [Entry(n='entry1', v=1.0),
                   Entry(n='entry2', v=2.0,
                         u='unit2', t=234)]
        m = Message(bn='name', e=entries)
        encoded_msg = json.loads(senml.encode_json(m))
        self.assertEqual(m.as_dict(), encoded_msg)


class TestDecodeXML(unittest.TestCase):

    def test_broken_message(self):
        xml = (
            '<?xml version="1.0" encoding="UTF-8"?>'
            '<senml xmlns="urn:ietf:params:xml:ns:senml" '
            'bt="5" ver="1">'
            '</senml>'.encode('UTF-8'))
        self.assertRaises(ValueError, senml.decode_xml, xml)

    def test_minimal_message(self):
        xml = (
            '<?xml version="1.0" encoding="UTF-8"?>'
            '<senml xmlns="urn:ietf:params:xml:ns:senml" '
            'bt="5" ver="1">'
            '<e n="name1" u="unit1" v="1.2"/>'
            '</senml>'.encode('UTF-8'))
        m = senml.decode_xml(xml)
        self.assertEqual(m.time, 5)
        self.assertEqual(len(m), 1)
        self.assertEqual(m[0].name, 'name1')
        self.assertEqual(m[0].unit, 'unit1')
        self.assertEqual(m[0].value, 1.2)
        self.assertEqual(m.version, 1)


class TestMessageParse(unittest.TestCase):

    def test_minimal_message(self):
        m = Message.parse({'e': [{'v': 1.0}]})
        self.assertEqual(len(m), 1)
        self.assertIsNone(m.name)
        self.assertIsNone(m.time)
        self.assertIsNone(m.unit)
        self.assertEqual(m.version, 1)

    def test_base_values(self):
        m = Message.parse({'bn': 'name', 'bt': 123, 'bu': 'unit',
                           'e': [{'s': 1.0}]})
        self.assertEqual(m.name, 'name')
        self.assertEqual(m.time, 123)
        self.assertEqual(m.unit, 'unit')
        self.assertEqual(m.version, 1)

    def test_version_warning(self):
        obj = {'e': [{'s': 1.0}]}
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            for v in [1, 1.0, 0.1, '1.0']:
                obj['ver'] = v
                Message.parse(obj)
            self.assertEqual(len(w), 2)


class TestMessage(unittest.TestCase):

    def test_empty_message(self):
        """Test default values as specified in RFC."""
        m = Message()
        self.assertEqual(len(m), 0)
        self.assertIsNone(m.name)
        self.assertIsNone(m.time)
        self.assertEqual(m.version, 1)
        self.assertEqual(len(m), 0)

    def test_base_values(self):
        """Test arguments are correctly used."""
        values = {'bn': 'name', 'bt': 123, 'bu': 'unit'}
        m = Message(**values)
        self.assertEqual(m.name, values['bn'])
        self.assertEqual(m.time, values['bt'])
        self.assertEqual(m.unit, values['bu'])
        self.assertEqual(m.version, 1)
        self.assertEqual(len(m), 0)

    def test_as_dict(self):
        """Test that only attributes with not None values are returned."""
        values = {'bn': 'name', 'bt': 1234}
        self.assertEqual(values, Message(**values).as_dict())

    def test_add_entry(self):
        m = Message()
        m.append(Entry)
        self.assertEqual(len(m), 1)

    def test_version(self):
        with warnings.catch_warnings(record=True) as w:
            warnings.simplefilter("always")
            # Invalid values
            m = Message(ver=0)
            m.version = 1.0
            # Valid values
            m.version = None
            m.version = 1
            self.assertEqual(len(w), 2)

    def test_message_length_is_number_of_entries(self):
        entries = [Entry(n='entry1'), Entry(n='entry2')]
        m = Message(e=entries)
        self.assertEqual(len(entries), len(m))


class TestMessageIterator(unittest.TestCase):

    def setUp(self):
        self.entries = [Entry(n='entry1', v=1.0),
                        Entry(n='entry2', v=1.0, u='unit1', t=234)]

    def test_name(self):
        m = Message(bn="name", e=self.entries)
        for e in m:
            if 'entry1' in e.name:
                self.assertEqual(e.name, 'nameentry1')
            if 'entry2' in e.name:
                self.assertEqual(e.name, 'nameentry2')

    def test_unit(self):
        m = Message(bu="bunit", e=self.entries)
        for e in m:
            if 'entry1' in e.name:
                self.assertEqual(e.unit, 'bunit')
            if 'entry2' in e.name:
                self.assertEqual(e.unit, 'unit1')

    def test_no_basetime(self):
        m = Message(bt=None, e=self.entries)
        now = int(time.time())
        with patch.object(time, 'time', return_value=now):
            for e in m:
                if 'entry1' in e.name:
                    self.assertTrue(e.time == now)
                if 'entry2' in e.name:
                    self.assertEqual(e.time, 234)

    def test_basetime(self):
        m = Message(bt=123, e=self.entries)
        for e in m:
            if 'entry1' in e.name:
                self.assertEqual(e.time, 123)
            if 'entry2' in e.name:
                self.assertEqual(e.time, 357)

    def test_sum_of_time(self):
        m = Message(bt=-234, e=self.entries)
        now = int(time.time())
        with patch.object(time, 'time', return_value=now):
            for e in m:
                if 'entry1' in e.name:
                    self.assertEqual(e.time, now-234)
                if 'entry2' in e.name:
                    self.assertEqual(e.time, now)


class TestEntryParse(unittest.TestCase):

    def test_mutex_values(self):
        objects = [
            {'v': 1.0, 'bv': True},
            {'v': 1.0, 'sv': 'str'},
            {'sv': 'str', 'bv': True},
            {'v': 1.0, 'sv': 'str', 'bv': True}
        ]
        for obj in objects:
            self.assertRaises(SchemaError, Entry.parse, obj)

    def test_non_mutex_summary_value(self):
        objects = [
            {'s': 1.0},
            {'s': 1.0, 'v': 1.1},
            {'s': 1.0, 'bv': True},
            {'s': 1.0, 'sv': 'str'}
        ]
        for obj in objects:
            self.assertIsInstance(Entry.parse(obj), Entry)

    def test_value_type(self):
        self.assertIsInstance(Entry.parse({'v': 1}).value, float)
        self.assertIsInstance(Entry.parse({'v': '1'}).value, float)
        self.assertIsInstance(Entry.parse({'v': 1.1}).value, float)
        self.assertIsInstance(Entry.parse({'v': '1.1'}).value, float)


class TestEntry(unittest.TestCase):

    def test_empty_entry(self):
        e = Entry()
        self.assertIsNotNone(e)

    def test_set_values(self):
        e = Entry()
        e.value = 1
        self.assertEqual(e.value, 1)
        e.bool_value = 1
        self.assertTrue(e.bool_value)
        self.assertIsNone(e.value)

    def test_empty_entry_as_dict(self):
        self.assertEqual({}, Entry().as_dict())

    def test_as_dict(self):
        """Test that only attributes with not None values are returned."""
        values = [
            {'n': 'name', 'v': 1.0, 't': 123, 'ut': 456},
            {'n': 'name', 'u': 'unit', 'bv': True, 's': 123}
        ]
        self.assertEqual(values[0], Entry(**values[0]).as_dict())
        self.assertEqual(values[1], Entry(**values[1]).as_dict())
        self.assertNotEqual(values[0], Entry(**values[1]).as_dict())
