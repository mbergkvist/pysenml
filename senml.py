import json
import time
import warnings
from xml.etree import ElementTree

from schema import Schema, Use, Or, SchemaError, And, Optional


def coalesce(*arg):
    return next((a for a in arg if a is not None), None)


def map_attributes(obj, mapping):
    """Map available and not None attributes in obj to new keys in mapping."""
    return {v: getattr(obj, k) for k, v in mapping.items()
            if getattr(obj, k, None) is not None}


class Entry(object):

    def __init__(self, n=None, u=None, v=None, sv=None, bv=None, s=None,
                 t=None, ut=None):
        self.name = n
        self.unit = u
        self.value = v
        self.string_value = sv
        self.bool_value = bv
        self.sum = s
        self.time = t
        self.update_time = ut

    def __repr__(self):
        return 'Entry(n=%r, u=%r, v=%r, sv=%r, bv=%r, s=%r, t=%r, ut=%r)' % (
            self.name, self.unit, self.value, self.string_value,
            self.bool_value, self.sum, self.time, self.update_time)

    def as_dict(self):
        """Return entry as a dictionary.

        Only attributes that is not None will be included.
        """
        mapping = {'name': 'n', 'unit': 'u', 'value': 'v',
                   'string_value': 'sv', 'bool_value': 'bv', 'sum': 's',
                   'time': 't', 'update_time': 'ut'}
        return map_attributes(self, mapping)

    def clear_values(self):
        self._value = None
        self._string_value = None
        self._bool_value = None

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, v):
        if v is None:
            self._value = None
        else:
            self.clear_values()
            self._value = float(v)

    @property
    def string_value(self):
        return self._string_value

    @string_value.setter
    def string_value(self, v):
        if v is None:
            self._string_value = None
        else:
            self.clear_values()
            self._string_value = str(v)

    @property
    def bool_value(self):
        return self._bool_value

    @bool_value.setter
    def bool_value(self, v):
        if v is None:
            self._bool_value = None
        else:
            self.clear_values()
            self._bool_value = bool(v)

    @staticmethod
    def parse(obj):
        mutex_keys = set(['v', 'sv', 'bv'])
        mutex_elements = Or(
            lambda x: len(mutex_keys - set(x.keys())) == 3 and 's' in x,
            lambda x: len(mutex_keys - set(x.keys())) == 2,
            error="'v', 'sv' and 'bv' are mutual exclusive but one "
                  "must be present unless 's' is present."
        )

        schema = Schema(
            And(
                mutex_elements,
                {
                    Optional('n', default=None): Use(str),
                    Optional('u', default=None): Use(str),
                    Optional('v', default=None): Use(float),
                    Optional('sv', default=None): Use(str),
                    Optional('bv', default=None): Use(bool),
                    Optional('s', default=None): Use(float),
                    Optional('t', default=0): Use(int),
                    Optional('ut', default=None): Use(int)
                },
                Use(lambda obj: Entry(**obj))
            ),
        )
        return schema.validate(obj)


class Message(object):

    def __init__(self, e=None, bn=None, bt=None, bu=None, ver=None):
        self.entries = coalesce(e, list())
        self.name = bn
        self.time = bt
        self.unit = bu
        self.version = ver
        self.created = int(time.time())

    def _adjusted_entry_time(self, entry):
        adjusted_time = coalesce(entry.time, 0) + coalesce(self.time, 0)
        if adjusted_time <= 0:
            adjusted_time += self.created
        return int(adjusted_time)

    def __getitem__(self, key):
        return self.entries[key]

    def __iter__(self):
        """Return an expanded copy of each entry.

        The name of an expanded entry is prefixed with the base name of the
        message, the time is the sum of the entry time and base time
        and the unit is copied from the message if it is missing from the
        original entry.
        """

        for entry in self.entries:
            e = Entry(
                n="{0}{1}".format(coalesce(self.name, ''),
                                  coalesce(entry.name, '')),
                u=coalesce(entry.unit, self.unit),
                v=entry.value,
                sv=entry.string_value,
                bv=entry.bool_value,
                s=entry.sum,
                t=self._adjusted_entry_time(entry),
                ut=entry.update_time)
            yield e

    def __len__(self):
        return len(self.entries)

    def __repr__(self):
        return "Message(e=%r, bn=%r, bt=%r, bu=%r, ver=%r)" % (
            self.entries, self.name, self.time, self.unit, self.version)

    def append(self, entry):
        self.entries.append(entry)

    def as_dict(self):
        """Return message as a dictionary.

        Only attributes that is not None will be included.
        """
        mapping = {'name': 'bn', 'time': 'bt', 'unit': 'bu', '_version': 'ver'}
        obj = map_attributes(self, mapping)
        if self.entries:
            obj['e'] = [entry.as_dict() for entry in self.entries]
        return obj

    @property
    def version(self):
        if self._version is None:
            return 1
        return self._version

    @version.setter
    def version(self, v):
        if v is not None and (not isinstance(v, int) or v < 1):
            warnings.warn('"ver" should be a positive number.')
        self._version = v

    @staticmethod
    def parse(obj):
        schema = Schema(
            And(
                {
                    "e": And(
                        lambda x: len(x) > 0,
                        [Use(Entry.parse)],
                    ),
                    Optional('bn', default=None): Use(str),
                    Optional('bt', default=None): Use(int),
                    Optional('bu', default=None): Use(str),
                    Optional('ver', default=None): Or(Use(int), Use(float))
                },
                Use(lambda obj: Message(**obj))
            )
        )
        return schema.validate(obj)


def decode_json(data):
    obj = json.loads(data)
    try:
        msg = Message.parse(obj)
    except(SchemaError) as e:
        raise ValueError(e)
    return msg


def encode_json(msg, indent=None):
    obj = msg.as_dict()
    return json.dumps(obj, sort_keys=True, indent=indent)


def decode_xml(data):
    root = ElementTree.fromstring(data)
    msg_root = dict(root.attrib)
    namespace = {'senml': 'urn:ietf:params:xml:ns:senml'}
    msg_root['e'] = [e.attrib for e in root.findall('senml:e', namespace)]
    try:
        msg = Message.parse(msg_root)
    except(SchemaError) as e:
        raise ValueError(e)
    return msg
