from setuptools import setup

setup(
    name="pysenml",
    version="0.1",
    author="Markus Bergkvist",
    author_email="markus@familjenbergkvist.net",
    description="SenML parser",
    license="ISC",
    keywords="senml parser",
    py_modules=['senml', 'schema']
)
